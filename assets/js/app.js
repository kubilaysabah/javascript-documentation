app = {
    init: function () {
        this.afterInit();
        this.eventListeners();
    },

    afterInit: function () {
        this.slider();
        this.blog();
    },

    slider: function () {
        $(".slider").slick({
            infinite: false,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            arrows: true,
            nextArrow: '<div class="slick-next"><i class="fas fa-chevron-right"></i></div>',
            prevArrow: '<div class="slick-prev"><i class="fas fa-chevron-left"></i></div>'
        });
    },

    navigationMenu : function() {
        
    },

    blog: function () {
        $(".blog").slick({
            infinite: false,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            arrows: true,
            nextArrow: '<div class="slick-next"><i class="fas fa-chevron-right"></i></div>',
            prevArrow: '<div class="slick-prev"><i class="fas fa-chevron-left"></i></div>',
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false
                    }
                }, 
                {

                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {

                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    },

    eventListeners: function() {
        $('[data-selector="mobile-menu"]').click(function(){
            $('body').toggleClass('mobile-menu-active overlay-active');
            $('#overlay').toggleClass('d-block');
        });

        $('#overlay').click(function(){
            $('body').removeClass();
            $(this).removeClass();
        });
    }
}

$(function () {
    app.init();
});